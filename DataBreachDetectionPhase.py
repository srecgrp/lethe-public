import random  # to shuffle the database
import string  # ascii_lowercase
import CryptographicallySecureRandomNumberGenerator as Rc  # the C's CSRNG
import gc  # To discard the seed
import sqlite3  # Database Purposes

rc = 0  # global variable for the random value released from Rc

# Global variables to keep track of attacks
# --------------------------------- For evaluation purposes --------------------------------- #
succAttacks = 0
detectedAttacks = 0
# ------------------------------------------------------------------------------------------- #


blacklist = []  # a list where the accounts under which a data breach has been detected are stored

epoch = 0  # keep track of number of epochs done

# create the database by creating a connection object
connection = sqlite3.connect("honeywords.db")
# create a cursor object in order to execute queries
cursor = connection.cursor()


# Get the seed from the trusted bootstrap and initialize the Rc
# Then, discard the seed
def getSeed(seed):
    global rc
    rc = Rc.rc(seed) % Rc.LIMIT
    del seed
    gc.collect()


# This function simulates a login for the given user, using the
# given password
def simulate_login(user, password, rs):
    # get the slots for this user from the checker's database
    swsC = cursor.execute("SELECT Honeyword_0, Honeyword_1, Honeyword_2, Honeyword_3,"
                          "Honeyword_4, Honeyword_5, Honeyword_6, Honeyword_7, Honeyword_8,"
                          "Honeyword_9, Honeyword_10, Honeyword_11, Honeyword_12, Honeyword_13,"
                          "Honeyword_14, Honeyword_15, Honeyword_16, Honeyword_17, Honeyword_18,"
                          "Honeyword_19 FROM cheggHoneywordsChecker WHERE ID= {}".format(user)).fetchall()
    swsC = list(swsC[0])

    # the position of the password/slot given, is the position of the real password in S's database
    real_position = swsC.index(password)

    sws = cursor.execute("SELECT Honeyword_0, Honeyword_1, Honeyword_2, Honeyword_3,"
                         "Honeyword_4, Honeyword_5, Honeyword_6, Honeyword_7, Honeyword_8,"
                         "Honeyword_9, Honeyword_10, Honeyword_11, Honeyword_12, Honeyword_13,"
                         "Honeyword_14, Honeyword_15, Honeyword_16, Honeyword_17, Honeyword_18,"
                         "Honeyword_19 FROM cheggHoneywords WHERE ID= {}".format(user)).fetchall()
    sws = list(sws[0])

    # extract the real password
    password = sws[real_position]

    # remove pass used, shuffle rest, insert pass in place given by Rs
    sws.remove(password)
    random.shuffle(sws)
    sws.insert(rs, password)

    for j in range(len(sws)):
        sws[j] = sws[j].replace("'", "''")

    # update row with new shuffled row
    connection.execute("UPDATE cheggHoneywords set "
                       "Honeyword_0 = \'{}\',"
                       "Honeyword_1 = \'{}\',"
                       "Honeyword_2 = \'{}\',"
                       "Honeyword_3 = \'{}\',"
                       "Honeyword_4 = \'{}\',"
                       "Honeyword_5 = \'{}\',"
                       "Honeyword_6 = \'{}\',"
                       "Honeyword_7 = \'{}\',"
                       "Honeyword_8 = \'{}\',"
                       "Honeyword_9 = \'{}\',"
                       "Honeyword_10 = \'{}\',"
                       "Honeyword_11 = \'{}\',"
                       "Honeyword_12 = \'{}\',"
                       "Honeyword_13 = \'{}\',"
                       "Honeyword_14 = \'{}\',"
                       "Honeyword_15 = \'{}\',"
                       "Honeyword_16 = \'{}\',"
                       "Honeyword_17 = \'{}\',"
                       "Honeyword_18 = \'{}\',"
                       "Honeyword_19 = \'{}\'"
                       "WHERE ID = {}".format(sws[0], sws[1], sws[2], sws[3], sws[4],
                                              sws[5], sws[6], sws[7], sws[8], sws[9],
                                              sws[10], sws[11], sws[12], sws[13],
                                              sws[14], sws[15], sws[16], sws[17],
                                              sws[18], sws[19], user))

    connection.commit()

    return real_position


# This function resets the row in the Checker's database in case of a
# password reset, by updating the row from slots 'a' through 't'
def resetRow(user_id):
    alphabet_string = string.ascii_lowercase
    sws = list(alphabet_string)

    connection.execute("UPDATE cheggHoneywordsChecker set "
                       "Honeyword_0 = \'{}\',"
                       "Honeyword_1 = \'{}\',"
                       "Honeyword_2 = \'{}\',"
                       "Honeyword_3 = \'{}\',"
                       "Honeyword_4 = \'{}\',"
                       "Honeyword_5 = \'{}\',"
                       "Honeyword_6 = \'{}\',"
                       "Honeyword_7 = \'{}\',"
                       "Honeyword_8 = \'{}\',"
                       "Honeyword_9 = \'{}\',"
                       "Honeyword_10 = \'{}\',"
                       "Honeyword_11 = \'{}\',"
                       "Honeyword_12 = \'{}\',"
                       "Honeyword_13 = \'{}\',"
                       "Honeyword_14 = \'{}\',"
                       "Honeyword_15 = \'{}\',"
                       "Honeyword_16 = \'{}\',"
                       "Honeyword_17 = \'{}\',"
                       "Honeyword_18 = \'{}\',"
                       "Honeyword_19 = \'{}\'"
                       "WHERE ID = {}".format(sws[0], sws[1], sws[2], sws[3], sws[4], sws[5], sws[6], sws[7],
                                              sws[8], sws[9], sws[10], sws[11], sws[12], sws[13], sws[14],
                                              sws[15], sws[16], sws[17], sws[18], sws[19],
                                              user_id))
    connection.commit()


# This function does theData Breach Detection phase for each login in L.txt
def dataBreachDetection():
    global rc, succAttacks, detectedAttacks, epoch

    # Keep track of attacks during this epoch
    # --------------------------------- For evaluation purposes --------------------------------- #
    succAttack = 0
    detectedAttack = 0
    # ------------------------------------------------------------------------------------------- #

    # L.txt contains a log with all the users (ids) that logged in during this epoch
    f = open("L.txt", 'r')
    lines = f.read().splitlines()
    f.close()

    # a dictionary to store each user's last used slot
    last_used_sw = {}

    # for every login in L.txt
    for line in lines:
        line = line.split(',')

        user_id = line[0]
        pass_initial_position = line[1]
        action = line[2]

        # if the action is password reset then reset the row and
        # remove the user and last used slot from the dictionary
        if action == "pReset":
            resetRow(user_id)
            if user_id in last_used_sw:
                last_used_sw.pop(user_id)

        # get their slots from checker's
        sws = cursor.execute("SELECT Honeyword_0, Honeyword_1, Honeyword_2, Honeyword_3,"
                             "Honeyword_4, Honeyword_5, Honeyword_6, Honeyword_7, Honeyword_8,"
                             "Honeyword_9, Honeyword_10, Honeyword_11, Honeyword_12, Honeyword_13,"
                             "Honeyword_14, Honeyword_15, Honeyword_16, Honeyword_17, Honeyword_18,"
                             "Honeyword_19 FROM cheggHoneywordsChecker WHERE ID= {}".format(user_id)).fetchall()
        sws = list(sws[0])

        # the slot used is the one at the initial position in the database of the checker
        # since checker's database has not been updated since the last epoch
        real_password = sws[int(pass_initial_position)]

        # if the user is int he dictionary
        if user_id in last_used_sw:
            # if last used sw is not the same as the one used then there has been a data breach
            if last_used_sw[user_id] != real_password:
                blacklist.append(int(user_id))
                # print("breach\n")
                f = open("breaches.txt", "a")
                f.write("Data Breach\n")
                f.write("user: " + str(user_id))
                f.write("\n")
                f.write("last used pass: " + str(last_used_sw[user_id]))
                f.write("\n")
                f.write("real pass: " + str(real_password))
                f.write("\n")
                f.close()
                detectedAttack += 1
            else:
                # there was a successful attack (if we are running random logins)
                # print("user {} all good\n".format(user_id))
                succAttack += 1
        else:
            # add the user to the dicionary along with their used slot
            # print("user {} added to last used\n".format(user_id))
            last_used_sw[user_id] = real_password

        # remove the slot used, shuffle the rest of the slots and insert the
        # slot used at Rc as it was retrieved from checker's CSRNG
        sws.remove(real_password)
        random.shuffle(sws)
        sws.insert(rc, real_password)

        for j in range(len(sws)):
            sws[j] = sws[j].replace("'", "''")
        # update the slots in the checker's database
        connection.execute("UPDATE cheggHoneywordsChecker set "
                           "Honeyword_0 = \'{}\',"
                           "Honeyword_1 = \'{}\',"
                           "Honeyword_2 = \'{}\',"
                           "Honeyword_3 = \'{}\',"
                           "Honeyword_4 = \'{}\',"
                           "Honeyword_5 = \'{}\',"
                           "Honeyword_6 = \'{}\',"
                           "Honeyword_7 = \'{}\',"
                           "Honeyword_8 = \'{}\',"
                           "Honeyword_9 = \'{}\',"
                           "Honeyword_10 = \'{}\',"
                           "Honeyword_11 = \'{}\',"
                           "Honeyword_12 = \'{}\',"
                           "Honeyword_13 = \'{}\',"
                           "Honeyword_14 = \'{}\',"
                           "Honeyword_15 = \'{}\',"
                           "Honeyword_16 = \'{}\',"
                           "Honeyword_17 = \'{}\',"
                           "Honeyword_18 = \'{}\',"
                           "Honeyword_19 = \'{}\'"
                           "WHERE ID = {}".format(sws[0], sws[1], sws[2], sws[3], sws[4], sws[5], sws[6], sws[7],
                                                  sws[8], sws[9], sws[10], sws[11], sws[12], sws[13], sws[14],
                                                  sws[15], sws[16], sws[17], sws[18], sws[19],
                                                  user_id))

        # get the next random value from the checker's CSRNG
        rc = Rc.rc_rest() % Rc.LIMIT
        connection.commit()

    f = open("L.txt", "w")
    # simulate logins for users that did only one login
    for user in last_used_sw:
        # get the next random value from the authenticator's CSRNG
        rs = Rc.rs_rest() % Rc.LIMIT
        # simulate login for that user using the last used slot as the password
        position = simulate_login(user, last_used_sw[user], rs)
        # add the user along with the position of the sweetword used in L.txt
        f.write(user)
        f.write(',')
        f.write(str(position))
        f.write(',')
        f.write('login')
        f.write("\n")
    f.close()

    # Keep track of attacks during this epoch
    # ---------------------------------------- For evaluation purposes ---------------------------------------- #
    if epoch > 0:
        totalEpoch = succAttack + detectedAttack
        print("Successful Attacks: {} / {}, {}%".format(succAttack, totalEpoch, (succAttack / totalEpoch) * 100))
        print("Detected Attacks: {} / {}, {}%".format(detectedAttack, totalEpoch, (detectedAttack / totalEpoch) * 100))
        print()

        succAttacks += succAttack
        detectedAttacks += detectedAttack
        total = succAttacks + detectedAttacks
        print("So far:")
        print(
            "Successful Attacks: {} / {}, {}%".format(succAttacks, total, (succAttacks / total) * 100))
        print(
            "Detected Attacks: {} / {}, {}%".format(detectedAttacks, total,
                                                    (detectedAttacks / total) * 100))
        print()
        print()
    epoch += 1
    # ---------------------------------------------------------------------------------------------------------- #

    del last_used_sw
    gc.collect()

    return blacklist


def reset():
    global rc, succAttacks, detectedAttacks, blacklist, epoch
    rc = 0
    succAttacks = 0
    detectedAttacks = 0
    blacklist = []
    epoch = 0
