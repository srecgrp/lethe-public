
# README #

This repo contains the prototype for the paper: **"Lethe: Practical Data Breach Detection with Zero Persistent Secret State"**.
As shown below, there are three parts for the simulation. The first part is for setting up the virtual environment and installing requirements. The second part is for training the honeywords generation model ([HoneyGen](#honeygen)) and producing the sweetwords list for each given password. The third part is for simulating [Lethe](#lethe)'s operation with a pre-created password file F that contains the sweetwords lists for a portion of chegg.com leaked database.


# Table of Contents
1. [How do I get set up?](#how-do-i-get-set-up?)
2. [HoneyGen](#honeygen)
3. [Lethe](#lethe)


### How do I get set up?

1. Clone the repo and open a terminal in the cloned folder.

2. Create a virtual environment in the cloned repo folder:
	* ```virtualenv -p python3.7.1 my_env```
3. Activate virtual environment.
	* ```source my_env/bin/activate```
4. Make sure python 3.7.1 is installed by issuing:
	* ```python --version```
5. Install all dependencies by issuing:
	* 	```pip3 install -r requirements.txt```


### HoneyGen

* Training of the password embeddigns model (issue only once!)
	- `python3 FastText.py` and wait until the training of the model finishes (size of the produced model ~12GB).

* Execute this step to roduce 20 honeywords for any given password:
	- `python3 HoneyGen_hybrid.py` and follow the on-screen instructions.



### Lethe

Issue Lethe with a pre-created password file F, using the chegg.com leaked passswords.
The simulation executes two experiments, one  with 2000 logins and another one with 4000 logins.
At the end of the simulation a plot with the successful login attempts, i.e., attempts with the correct password, will be shown.
The attack success rate should be approximately 5%, which is the success rate of the classic honeywords schema, given that 20 sweetwords are used for each user account.


#### How to run the simulation?
Issue: `python3 simulateLogins.py`

-For altering the number of users (max=50000), change the global variable "USERS" in bootstrap.py  
-For setting the number of logins in te entire simulation, change the global variable "iterations" in simulateLogins.py  
-For setting the number of logins in each epoch, change the global variable "EPOCHS" in DataBreachDetectionPhase.py  

See below a detailed explanation for each subscript.


#### Summary of Lethe's subscripts

**bootstrap.py:**                                       This script creates the table cheggHoneywords and cheggHoneywordsChecker in the
                                                    honeywords.db (which is sqlite3). It fills the tables with the username, the correct
                                                    password and the 19 sweetwords found in "chegg-com_sorted_preprocessed.txt". Then,
                                                    it simulates the correct login for each user to (i.e., the registration phase for
                                                    each user) to shuffle the databases for the first time and add each user in L.txt
                                                    (login event thus the authentication phase is triggered). Also, this script does
                                                    the trusted bootstrap, which is basically the initialization of the CSRNG of each
                                                    server, with the same seed. Therefore the two CSRNGs are synchronized.

**CryptographicallySecureRandomNumberGenerator.py:**    This script implements the RC4. This CSRNG is used to get the next position of the
                                                    password used by the user. The two servers (C & S) have a CSRNG each, and they are
                                                    synchronized.

**DataBreachDetectionPhase.py:**                        This script checks if there has been a data breach given an L.txt with the logins
                                                    that happened (user, used_sw_position, action). Initially S' & C's database are
                                                    untouched. But after the simulation of logins, S's database is shuffled since the
                                                    used sw is placed in a position given by the Rs. This script will simulate the same
                                                    shuffle, at C's database using its own Rc, which has the same seed and therefore
                                                    should do the same changes as S (since the two number generators are synchronised).
                                                    If one of the passwords are not the last one used then there has been a data breach.
                                                    Also, if any user has not logged in more than  once, we simulate one login for that
                                                    user, to be checked in the next epoch.

**AuthenticationPhase.py:**                         For each login event, if the password used is one of the sweetwords in the database
                                                    then the system approves access else it declines. If the access is approved then
                                                    the sweetword used is placed in Rs (a value derived from S's CSRNG), and the rest
                                                    of the sweetwords are shuffled. For each login, S record those login in L.txt in the
                                                    format (user ID, initial position of password, action). At the end of each epoch, the
                                                    Data Breach Detection occurs.

**simulateLogins.py:**                                  This script simulates correct and random logins to check the functionality of the Data
                                                    Breach Detection Phase implementation
                                                    For the correct logins, it chooses randomly 'iterations' users and gets their correct
                                                    passwords from "chegg-com_sorted_preprocessed.txt" where they are known.
                                                    For the random logins, it chooses randomly 'iterations' users and gets their passwords
                                                    choosing randomly one sweetword for that user in the database.
                                                    For both cases, each login triggers the authentication phase.


### Who do I talk to? ###

* Having problems? Email dionysiou.antreas@ucy.ac.cy for [HoneyGen](#honeygen) and anastasiou.chrystalla@ucy.ac.cy for [Lethe](#lethe).

### Citing this work ###

If you use this repository for academic research, you are highly encouraged to cite our papers:

HoneyGen:
```
@inproceedings{dionysiou2021honeygen,
  title={HoneyGen: Generating Honeywords Using Representation Learning},
  author={Dionysiou, Antreas and Vassiliades, Vassilis and Athanasopoulos, Elias},
  booktitle={Proceedings of the 2021 ACM Asia Conference on Computer and Communications Security},
  pages={265--279},
  year={2021}
}
```

Lethe:
```
@inproceedings{dionysiou2022lethe,
  title={Lethe: Practical Data Breach Detection with Zero Persistent Secret State},
  author={Dionysiou, Antreas and Athanasopoulos, Elias},
  booktitle={2022 IEEE 7th European Symposium on Security and Privacy (EuroS\&P)},
  pages={223--235},
  year={2022},
  organization={IEEE}
}
```
