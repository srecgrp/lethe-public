# Global variable to easily change the number of sweetwords for each user, thus
# the range in which the values the CSRNG returns will be
LIMIT = 20

# Authentication Server S's variables
S_global = []
i_global = 0
j_global = 0

# Checking Server's C variables
S_global_checker = []
i_global_c = 0
j_global_c = 0


def KSA(key):
    keylength = len(key)

    # Initially we have one list S that has all bytes from 0 to 255
    S = list(range(256))

    j = 0
    # Initial permutation of S
    for i in range(256):
        j = (j + S[i] + key[i % keylength]) % 256
        S[i], S[j] = S[j], S[i]  # swap

    return S


# get one by one key
def PRGA_one_by_one():
    global i_global, j_global
    i_global = (i_global + 1) % 256
    j_global = (j_global + S_global[i_global]) % 256
    S_global[i_global], S_global[j_global] = S_global[j_global], S_global[i_global]  # swap

    # the next byte
    K = S_global[(S_global[i_global] + S_global[j_global]) % 256]
    return K


# get one by one key
def PRGA_one_by_one_c():
    global i_global_c, j_global_c
    i_global_c = (i_global_c + 1) % 256
    j_global_c = (j_global_c + S_global_checker[i_global_c]) % 256
    S_global_checker[i_global_c], S_global_checker[j_global_c] = S_global_checker[j_global_c], S_global_checker[
        i_global_c]  # swap

    # the next byte
    K = S_global_checker[(S_global_checker[i_global_c] + S_global_checker[j_global_c]) % 256]
    return K


def preparing_key_array(s):
    return [ord(c) for c in s]


# -------------------- Different for Authenticator S, so that they can be synchronised -------------------- #


# retrieve the first key, given the seed (for server S)
def rs(seed):
    global S_global
    seed = str(seed)
    seed = preparing_key_array(seed)
    S_global = KSA(seed)

    key = PRGA_one_by_one()
    return key


# retrieve the rest of keys (for server S)
def rs_rest():
    return PRGA_one_by_one()


# -------------------- Different for Checker C, so that they can be synchronised -------------------- #

# retrieve the first key, given the seed (for checker C)
def rc(seed):
    global S_global_checker
    seed = str(seed)
    seed = preparing_key_array(seed)
    S_global_checker = KSA(seed)

    key = PRGA_one_by_one_c()
    return key


# retrieve the rest of keys (for checker C)
def rc_rest():
    return PRGA_one_by_one_c()


def reset():
    global S_global, i_global, j_global, S_global_checker, i_global_c, j_global_c
    S_global = []
    i_global = 0
    j_global = 0

    S_global_checker = []
    i_global_c = 0
    j_global_c = 0
