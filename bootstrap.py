import sqlite3  # Database Purposes
import random  # To shuffle database
import os  # To remove files
import gc  # To discard the seed

import AuthenticationPhase as Authenticator  # To initialize Authenticator's CSRNG
import DataBreachDetectionPhase as Checker  # To initialize Checker's CSRNG
from datetime import datetime  # To initialize the seed randomly

# create the database by creating a connection object
connection = sqlite3.connect("honeywords.db")
# create a cursor object in order to execute queries
cursor = connection.cursor()

# A list to save all the real passwords for registration purposes
real_passwords = []

# Global variable to easily change the number of users to be inserted in the database
USERS = 10000


# This function initializes the system
# Each server initializes a cryptographically secure random number sequence
# Both servers use the same seed to initialize their CSRNGs and the seed is then discarded
# Consequently, the two CSRNGs (i.e., Rs and Rc) are synchronized.
def trustedBootstrap():
    # Calculate a true random seed based on the date
    seed = datetime.now()
    # Send seed to the checking server
    Checker.getSeed(seed)
    # Send seed to the authenticating server
    Authenticator.getSeed(seed)
    # Discard the seed
    del seed
    gc.collect()


# This function fills the database of the Authenticator with the real passwords and generated honeywords
# for chegg.com
# Also, it fills the database of the Checker with slots ('a' through 't')
# --------------------------------- For evaluation purposes --------------------------------- #
def initialBootstrap():
    # Drop the tables each time to recreate
    cursor.execute("DROP TABLE cheggHoneywords")
    cursor.execute("DROP TABLE cheggHoneywordsChecker")

    # Create the tables for each server respectively
    cursor.execute(
        "CREATE TABLE cheggHoneywords (ID INT PRIMARY KEY NOT NULL,Honeyword_0 TEXT, Honeyword_1 TEXT,"
        "Honeyword_2 TEXT, Honeyword_3 TEXT, Honeyword_4 TEXT,  Honeyword_5 TEXT, Honeyword_6 TEXT,  "
        "Honeyword_7 TEXT, Honeyword_8 TEXT, Honeyword_9 TEXT,  Honeyword_10 TEXT,  Honeyword_11 TEXT,"
        "Honeyword_12 TEXT, Honeyword_13 TEXT, Honeyword_14 TEXT,  Honeyword_15 TEXT,  Honeyword_16 TEXT,"
        "Honeyword_17 TEXT,  Honeyword_18 TEXT, Honeyword_19 TEXT)")
    cursor.execute(
        "CREATE TABLE cheggHoneywordsChecker (ID INT PRIMARY KEY NOT NULL,Honeyword_0 TEXT, Honeyword_1 TEXT,"
        "Honeyword_2 TEXT, Honeyword_3 TEXT, Honeyword_4 TEXT,  Honeyword_5 TEXT, Honeyword_6 TEXT,  "
        "Honeyword_7 TEXT, Honeyword_8 TEXT, Honeyword_9 TEXT,  Honeyword_10 TEXT,  Honeyword_11 TEXT,"
        "Honeyword_12 TEXT, Honeyword_13 TEXT, Honeyword_14 TEXT,  Honeyword_15 TEXT,  Honeyword_16 TEXT,"
        "Honeyword_17 TEXT,  Honeyword_18 TEXT, Honeyword_19 TEXT)")

    f = open("chegg-com_sorted_preprocessed.txt")
    lines = f.read().splitlines()
    k = 0
    # Fill the tables for USERS number of users
    for line in lines:
        if k < USERS:
            sws = line.split()
            query = "INSERT INTO cheggHoneywords VALUES ( {}, ".format(k)
            real_passwords.insert(k, sws[0])

            for i in range(0, len(sws)):
                # help with special characters
                sws[i] = sws[i].replace("'", "''")
                query = query + '\'' + sws[i] + '\''
                if i == len(sws) - 1:
                    query = query + ')'
                else:
                    query = query + ","
            cursor.execute(query)

            query = "INSERT INTO cheggHoneywordsChecker VALUES ( {}, ".format(k)
            k = k + 1
            # did it with letters so that there are no issues with both the user id and the passwords being numbers
            c = 'a'
            for j in range(20):
                query = query + '\'' + c + '\''
                if j == 19:
                    query = query + ')'
                else:
                    query = query + ","
                c = chr(ord(c) + 1)
            cursor.execute(query)

        else:
            break
    connection.commit()


# This function shuffles the Authenticator's database randomly
# --------------------------------- For evaluation purposes --------------------------------- #
def shuffleDatabase():
    rows = cursor.execute("SELECT * FROM cheggHoneywords").fetchall()
    k = 0
    for sws in rows:
        sws = list(sws)
        sws.remove(k)
        random.shuffle(sws)
        sws.insert(0, k)

        for i in range(1, len(sws)):
            sws[i] = sws[i].replace("'", "''")

        connection.execute("UPDATE cheggHoneywords set "
                           "Honeyword_0 = \'{}\',"
                           "Honeyword_1 = \'{}\',"
                           "Honeyword_2 = \'{}\',"
                           "Honeyword_3 = \'{}\',"
                           "Honeyword_4 = \'{}\',"
                           "Honeyword_5 = \'{}\',"
                           "Honeyword_6 = \'{}\',"
                           "Honeyword_7 = \'{}\',"
                           "Honeyword_8 = \'{}\',"
                           "Honeyword_9 = \'{}\',"
                           "Honeyword_10 = \'{}\',"
                           "Honeyword_11 = \'{}\',"
                           "Honeyword_12 = \'{}\',"
                           "Honeyword_13 = \'{}\',"
                           "Honeyword_14 = \'{}\',"
                           "Honeyword_15 = \'{}\',"
                           "Honeyword_16 = \'{}\',"
                           "Honeyword_17 = \'{}\',"
                           "Honeyword_18 = \'{}\',"
                           "Honeyword_19 = \'{}\'"
                           "WHERE ID = {}".format(sws[1], sws[2], sws[3], sws[4], sws[5], sws[6], sws[7],
                                                  sws[8], sws[9], sws[10], sws[11], sws[12], sws[13], sws[14],
                                                  sws[15], sws[16], sws[17], sws[18], sws[19], sws[20], k))
        k += 1
        connection.commit()


# This function does a mass registration phase for each user
# inserted in the Authenticator's database.
# That is, for every login, the real password is used to log in the user to the system.
# The login sets off the authentication phase.
# --------------------------------- For evaluation purposes --------------------------------- #
def massRegistrationPhase():
    if os.path.exists("L.txt"):
        os.remove("L.txt")
    if os.path.exists("breaches.txt"):
        os.remove("breaches.txt")
    # The fist step is to initialize/sychronize the CSRNG of both servers
    trustedBootstrap()

    # Log in each user
    for i in range(len(real_passwords)):
        Authenticator.authenticate_registration(i, real_passwords[i])


def bootSystem():
    initialBootstrap()
    shuffleDatabase()
    massRegistrationPhase()

    # if connection:
    #     # using close() method, we will close
    #     # the connection
    #     connection.close()
    #
    #     # After closing connection object, we
    #     # will print "the sqlite connection is
    #     # closed"
    #     print("the sqlite connection is closed")


def reset():
    global real_passwords
    real_passwords = []
