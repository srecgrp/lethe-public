import CryptographicallySecureRandomNumberGenerator as Rs  # the S's CSRNG
import gc  # To discard the seed
import sqlite3  # Database Purposes
import random  # To shuffle database
import DataBreachDetectionPhase as Checker  # for the authentication phase
import bootstrap  # for USERS

# Global variable to easily change the frequency of the authentication phase i.e, EPOCH
EPOCHS = 1000

# create the database by creating a connection object
connection = sqlite3.connect("honeywords.db")
# create a cursor object in order to execute queries
cursor = connection.cursor()

rs = 0  # global variable for the random value released from Rs


# Get the seed from the trusted bootstrap and initialize the Rs
# Then, discard the seed
def getSeed(seed):
    global rs
    rs = Rs.rs(seed) % Rs.LIMIT
    del seed
    gc.collect()


# This function does the authentication phase for the given user IDS and passwords respectively
# For each user, the function checks if the password provided is one of the sweetwords in the
# database for that user, and if it is then it approves access, else it denies access.
# Every EPOCHS logins, the function calls the Data Breach detection phase.
def authenticationPhase(user_ids, passwords):
    global rs
    # a list where the accounts under which a data breach has been detected are stored
    blacklist = []
    # get the next random value from the CSRNG
    rs = Rs.rs_rest() % Rs.LIMIT

    # open the L.txt to store all the logins, for the Data Breach detection
    f = open("L.txt", "a")

    # For each login
    for i in range(len(user_ids)):
        if user_ids[i] in blacklist:
            continue
            # print("Found a breach under user: {}. Account is frozen, will not proceed.".format(user_ids[i]))
        else:
            # retrieve the user's sweetwords from S's database
            sws = cursor.execute("SELECT Honeyword_0, Honeyword_1, Honeyword_2, Honeyword_3,"
                                 "Honeyword_4, Honeyword_5, Honeyword_6, Honeyword_7, Honeyword_8,"
                                 "Honeyword_9, Honeyword_10, Honeyword_11, Honeyword_12, Honeyword_13,"
                                 "Honeyword_14, Honeyword_15, Honeyword_16, Honeyword_17, Honeyword_18,"
                                 "Honeyword_19 FROM cheggHoneywords WHERE ID= {}".format(user_ids[i])).fetchall()
            sws = list(sws[0])

            # if password given is one of the sweetwords
            if passwords[i] in sws:
                # get a random value between [0,1]
                rand = random.random()
                action = "login"
                # if the value is less than 0.3, mark the action as password reset,
                # else leave action as login
                if rand <= 0.2:
                    # user should have logged in at least once before to do a preset
                    logged = user_ids[0:i - 1]
                    if user_ids[i] in logged:
                        action = "pReset"
                # before shuffling row in database and placing the pass used at Rs position
                # write the user ID and the index of the password used in L.txt
                f.write(str(user_ids[i]))
                f.write(',')
                f.write(str(sws.index(passwords[i])))
                f.write(',')
                f.write(action)
                f.write("\n")

                # remove pass used, shuffle rest, insert pass in place given by Rs
                sws.remove(passwords[i])
                random.shuffle(sws)
                sws.insert(rs, passwords[i])

                for j in range(len(sws)):
                    sws[j] = sws[j].replace("'", "''")

                # update row with new shuffled row
                connection.execute("UPDATE cheggHoneywords set "
                                   "Honeyword_0 = \'{}\',"
                                   "Honeyword_1 = \'{}\',"
                                   "Honeyword_2 = \'{}\',"
                                   "Honeyword_3 = \'{}\',"
                                   "Honeyword_4 = \'{}\',"
                                   "Honeyword_5 = \'{}\',"
                                   "Honeyword_6 = \'{}\',"
                                   "Honeyword_7 = \'{}\',"
                                   "Honeyword_8 = \'{}\',"
                                   "Honeyword_9 = \'{}\',"
                                   "Honeyword_10 = \'{}\',"
                                   "Honeyword_11 = \'{}\',"
                                   "Honeyword_12 = \'{}\',"
                                   "Honeyword_13 = \'{}\',"
                                   "Honeyword_14 = \'{}\',"
                                   "Honeyword_15 = \'{}\',"
                                   "Honeyword_16 = \'{}\',"
                                   "Honeyword_17 = \'{}\',"
                                   "Honeyword_18 = \'{}\',"
                                   "Honeyword_19 = \'{}\'"
                                   "WHERE ID = {}".format(sws[0], sws[1], sws[2], sws[3], sws[4], sws[5], sws[6],
                                                          sws[7],
                                                          sws[8], sws[9], sws[10], sws[11], sws[12], sws[13], sws[14],
                                                          sws[15], sws[16], sws[17], sws[18], sws[19],
                                                          user_ids[i]))

                connection.commit()

            # if epoch has ended, check for data breach
            if (i + 1) % EPOCHS == 0:
                f.close()
                # return in blacklist, the accounts for which a data breach has been detected
                blacklist = Checker.dataBreachDetection().copy()
                # dataBreachDetection will overwrite L and move some checks for next checking
                f = open("L.txt", "a")

            # get next Rs and approve access to system since pass is in sweetwords
            rs = Rs.rs_rest() % Rs.LIMIT
            # print("Approve Access")
        # else:
        #     # decline access to system since pass given is not one of the sweetwords
        #     print("Decline Access")
        #     return

    f.close()


# This function does the first login for each user i.e., the registration of the user
def authenticate_registration(user_id, password):
    global rs

    # open the L.txt to store all the logins, for the Data Breach detection
    f = open("L.txt", "a")

    # get the sweetwords of the given user from S's database
    sws = cursor.execute("SELECT Honeyword_0, Honeyword_1, Honeyword_2, Honeyword_3,"
                         "Honeyword_4, Honeyword_5, Honeyword_6, Honeyword_7, Honeyword_8,"
                         "Honeyword_9, Honeyword_10, Honeyword_11, Honeyword_12, Honeyword_13,"
                         "Honeyword_14, Honeyword_15, Honeyword_16, Honeyword_17, Honeyword_18,"
                         "Honeyword_19 FROM cheggHoneywords WHERE ID= {}".format(user_id)).fetchall()
    sws = list(sws[0])

    # if password given is one of the sweetwords
    if password in sws:
        action = "login"
        # before shuffling row in database and placing the pass used at Rs position
        # write the user ID and the index of the password used in L.txt
        f.write(str(user_id))
        f.write(',')
        f.write(str(sws.index(password)))
        f.write(',')
        f.write(action)
        f.write("\n")

        # remove pass used, shuffle rest, insert pass in place given by Rs
        sws.remove(password)
        random.shuffle(sws)
        sws.insert(rs, password)

        for j in range(len(sws)):
            sws[j] = sws[j].replace("'", "''")

        # update row with new shuffled row
        connection.execute("UPDATE cheggHoneywords set "
                           "Honeyword_0 = \'{}\',"
                           "Honeyword_1 = \'{}\',"
                           "Honeyword_2 = \'{}\',"
                           "Honeyword_3 = \'{}\',"
                           "Honeyword_4 = \'{}\',"
                           "Honeyword_5 = \'{}\',"
                           "Honeyword_6 = \'{}\',"
                           "Honeyword_7 = \'{}\',"
                           "Honeyword_8 = \'{}\',"
                           "Honeyword_9 = \'{}\',"
                           "Honeyword_10 = \'{}\',"
                           "Honeyword_11 = \'{}\',"
                           "Honeyword_12 = \'{}\',"
                           "Honeyword_13 = \'{}\',"
                           "Honeyword_14 = \'{}\',"
                           "Honeyword_15 = \'{}\',"
                           "Honeyword_16 = \'{}\',"
                           "Honeyword_17 = \'{}\',"
                           "Honeyword_18 = \'{}\',"
                           "Honeyword_19 = \'{}\'"
                           "WHERE ID = {}".format(sws[0], sws[1], sws[2], sws[3], sws[4], sws[5], sws[6], sws[7],
                                                  sws[8], sws[9], sws[10], sws[11], sws[12], sws[13], sws[14],
                                                  sws[15], sws[16], sws[17], sws[18], sws[19],
                                                  user_id))

        connection.commit()

        # if epoch has ended, check for data breach
        if user_id == bootstrap.USERS:
            f.close()
            Checker.dataBreachDetection()
            # dataBreachDetection will overwrite L and move some checks for next checking
            f = open("L.txt", "a")
        else:
            # get next Rs and approve access to system since pass is in sweetwords
            rs = Rs.rs_rest() % Rs.LIMIT

        # print("Approve Access")
    else:
        # decline access to system since pass given is not one of the sweetwords
        print("Decline Access")
        return

    f.close()


def reset():
    global rs
    rs = 0
