import numpy as np
import sqlite3  # Database Purposes
import matplotlib.pyplot as plt  # For the plots


import AuthenticationPhase as Authenticator  # for Authentication Phase
import bootstrap  # for initial bootstrap of the system
import DataBreachDetectionPhase as Checker  # for resetting
import CryptographicallySecureRandomNumberGenerator as CSRNG  # for resetting

# Global variable to easily change the number of iterations/logins in this test
iterations = 2000
# For plotting
data = []
its = []

# create the database by creating a connection object
connection = sqlite3.connect("honeywords.db")
# create a cursor object in order to execute queries
cursor = connection.cursor()

TESTS = 2


# This function simulates correct logins "iterations" times, using random user IDs and their
# correct passwords, as retrieved from the file "chegg-com_sorted_preprocessed.txt"
# --------------------------------- For evaluation purposes --------------------------------- #
def simulateCorrectLogins():
    # Find total number of users
    number_of_users = cursor.execute("SELECT COUNT(*) FROM cheggHoneywords")
    for u in number_of_users:
        u = list(u)
        number_of_users = u[0]

    # Fill an array of "iterations" size, with random user IDs from 0 to total number of users
    user_ids = np.random.randint(number_of_users, size=iterations)

    # Open the file to get the correct passwords of each user
    f = open("chegg-com_sorted_preprocessed.txt", "r")
    lines = f.read().splitlines()
    f.close()
    correct_passes = []
    for user_id in user_ids:
        line = lines[user_id]
        sws = line.split()
        correct_passes.append(sws[0])

    # Simulate a login event for each user, which will each time trigger an authentication phase
    Authenticator.authenticationPhase(user_ids, correct_passes)


# This function simulates random logins "iterations" times, using random user IDs and their
# a random password between the 20 retrieved from Authentication Server S's database
# --------------------------------- For evaluation purposes --------------------------------- #
def simulateRandomLogins():
    # Find total number of users
    number_of_users = cursor.execute("SELECT COUNT(*) FROM cheggHoneywords")
    for u in number_of_users:
        u = list(u)
        number_of_users = u[0]

    # Fill an array of "iterations" size, with random user IDs from 0 to total number of users
    user_ids = np.random.randint(low=0, high=number_of_users, size=iterations)
    # Fill an array of "iterations" size, with random numbers from 1 to 20, which stands for the random position of
    # password of each user
    passes_positions = np.random.randint(low=1, high=20, size=iterations)

    # For each user, retrieve the random password from the authenticator's database
    passes = []
    for i in range(len(user_ids)):
        sws = cursor.execute("SELECT Honeyword_0, Honeyword_1, Honeyword_2, Honeyword_3,"
                             "Honeyword_4, Honeyword_5, Honeyword_6, Honeyword_7, Honeyword_8,"
                             "Honeyword_9, Honeyword_10, Honeyword_11, Honeyword_12, Honeyword_13,"
                             "Honeyword_14, Honeyword_15, Honeyword_16, Honeyword_17, Honeyword_18,"
                             "Honeyword_19 FROM cheggHoneywords WHERE ID= {}".format(user_ids[i])).fetchall()
        sws = list(sws[0])
        passes.append(sws[passes_positions[i]])

    # Simulate a login event for each user, which will each time trigger an authentication phase
    Authenticator.authenticationPhase(user_ids, passes)


# Testing function for login simulation
# --------------------------------- For evaluation purposes --------------------------------- #
def simulateLoginsTest():
    user_ids = list()
    user_ids.append(0)
    user_ids.append(1)
    user_ids.append(0)
    user_ids.append(2)
    f = open("chegg-com_sorted_preprocessed.txt", "r")
    lines = f.read().splitlines()
    f.close()
    correct_passes = list()
    for user_id in user_ids:
        line = lines[user_id]
        sws = line.split()
        correct_passes.append(sws[0])
    Authenticator.authenticationPhase(user_ids, correct_passes)


def fullSimulation():
    global iterations
    # Firstly, boot the system
    bootstrap.bootSystem()

    # simulateLoginsTest()
    # simulateCorrectLogins()
    # print("Done with correct logins")

    # Simulate random logins
    print("Start Random logins for {} logins:".format(iterations))
    simulateRandomLogins()
    print("Done with random logins for {} logins.\n".format(iterations))


if __name__ == '__main__':
    plt.style.use('ggplot')
    for i in range(TESTS):
        its.append(str(iterations))
        fullSimulation()
        iterations += 2000
        data.append((Checker.succAttacks / (Checker.succAttacks + Checker.detectedAttacks)) * 100)
        Checker.reset()
        Authenticator.reset()
        bootstrap.reset()
        CSRNG.reset()

    plt.bar(its, data, color='pink')
    plt.xlabel("Number of logins")
    plt.ylabel("Successful Attacks Percentage (%)")
    plt.title("Results of Successful Attacks Percentage\n", fontweight="bold")
    # plt.legend()
    avg = sum(data) / TESTS
    plt.axhline(avg, color='gray', linestyle='--')
    x_pos = [i for i, _ in enumerate(its)]
    plt.xticks(x_pos, its)
    plt.show(block=True)
